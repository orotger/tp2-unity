﻿using System.Collections;
using UnityEngine;

namespace Runtime
{
    /// <summary>
    /// Ce script permet à un objet de se déplacer de sa position d'origine à une destination.
    /// Il va aussi permettre à un objet de changer de couleur au cours du temps.
    /// </summary>
    public class MoveTo : MonoBehaviour
    {
	[Header("Général")]
	[Tooltip("Le temps que va prendre le déplacement")]
	[Range(0,2)]
	[SerializeField]
	private float m_Duration;

	[Header("Déplacement")]
        [Tooltip("La destination de l'objet")]
        [SerializeField]
	private Vector3 m_Goal;
        public Vector3 Goal
        {
            get => m_Goal;
            set => m_Goal = value;
        }

	[Header("Changement de couleur")]
        [Tooltip("La courbe utilisé pour faire le Lerp entre l'origine et l'arrivée")]
	[SerializeField] private AnimationCurve m_MovementLerpingCurve;
        
        
        [Tooltip("La couleur d'origine")]
        [SerializeField] private Color m_Color1;
        [Tooltip("La couleur finale")]
        [SerializeField] private Color m_Color2;

	[Space(10)]        
        [Tooltip("La courbe utilisé pour faire le Lerp entre l'origine et l'arrivée")]
        [SerializeField] private AnimationCurve m_ColorLerpingCurve;
        

        // Le renderer utilisé pour changer la couleur du material
        private Renderer m_Renderer;
        private Vector3 m_Origin;
        private float m_StartTime;

        private void Awake()
        {
            m_Renderer = GetComponent<Renderer>();
            m_Origin = transform.localPosition;
        }

        private void Start()
        {
            m_StartTime = Time.time;
            StartCoroutine(c_LerpToGoal());
            
            IEnumerator c_LerpToGoal()
            {
                float currentTime;
                do
                {
                    currentTime = Time.time - m_StartTime;
                    float ratio = currentTime / m_Duration;
                    UpdatePosition(ratio);
                    UpdateColor(ratio);
                    yield return new WaitForEndOfFrame();
                } while (currentTime <= m_Duration);
            }
        }

        private void UpdateColor(float _ratio)
        {
            m_Renderer.sharedMaterial.color = Color.LerpUnclamped(m_Color1, m_Color2, _ratio);
        }

        private void UpdatePosition(float _ratio)
        {
            transform.localPosition = Vector3.LerpUnclamped(m_Origin, m_Goal, _ratio);
        }
    }
}