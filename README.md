# Cours Ensicaen 3A
## Introduction à Unity
### TP2: Changer l'éditeur de Unity


## Introduction 
Dans ce tp, nous allons voir les bases du tooling dans Unity.

N'hésitez pas à me contacter à tout moment sur l'adresse <kubiak@ecole.ensicaen.fr> 
si vous êtes bloqués ou que vous avez une quelconque question. 

Notions acquises durant le tp:
- Utilisation des attributes pour rendre l'éditeur plus visuel
- Affichage d'un Handler dans la Scène
- Changement de l'éditeur d'un composant

Vous trouverez tout à la fin mes critères de notation.

## Présentation du projet
- La scène se nomme SampleScene et se trouve sous Assets/Resources/Scenes
- Dans la scène, il y a un cube qui porte le MonoBehaviour dont nous allons changer l'éditeur.
- Ce MonoBehaviour (MoveTo) va faire se déplacer le cube à une certaine position et changer sa couleur au cours du temps.

## Utilisation des attributes pour rendre son éditeur plus visuel
Pour l'instant nous avons:

![](./subject-screens/component-without-attributes.PNG)

Nous cherchons à obtenir:

![](./subject-screens/component-attributes.PNG)

Vous utiliserez les attributes vu en cours afin d'obtenir ce visuel. 
De plus, vous ferez en sorte que les commentaires au dessus des champs s'affichent quand vous passez la souris dessus.

<details>
<summary> Aide </summary>

- Header
- Tooltip
- Range
- Space
</details>


## Affichage d'un Handle dans la Scène
Maintenant nous allons faire en sorte qu'un handle apparaisse dans la scène et nous permettent de changer Goal.

![Un handle de déplacement](./subject-screens/handlePNG.PNG)

- Créez un Script Editor dans Scripts/Editor et appelez le MoveToEditor.cs
- Surchargez OnSceneGUI
- Utilisez Handles.PositionHandle pour afficher un Handle de position
- N'oubliez pas de permettre le Ctrl-Z grâce à Undo.RecordObject

<details>
<summary> Aide </summary>
- Vous pouvez vous inspirer de ce [tutoriel](https://docs.unity3d.com/Manual/editor-CustomEditors.html)
- Pour le [handle](https://docs.unity3d.com/ScriptReference/Handles.PositionHandle.html)
- Pour le [undo](https://docs.unity3d.com/ScriptReference/Undo.RecordObject.html). Vous passerez 'target' comme objet à sauvegarder.
</details>

## Bonus - Ajout d'une fonctionnalité pour pouvoir changer Goal rapidement
Maintenant, nous désirons pouvoir changer Goal ssu un des axes facilement. 
Pour cela, nous aurons un slider servant à indiquer la distance par rapport à l'origine et 
une une liste déroulante pour choisir la direction.
Sur clic d'un bouton Goal sera changé.

Le résultat final ressemblera à ça:
![](./subject-screens/final-result.PNG)

<details>
<summary> Aide </summary>
- Vous pouvez utiliser [EditorGUILayout.Slider](https://docs.unity3d.com/ScriptReference/EditorGUILayout.Slider.html)
- Vous pouvez utiliser [EditorGUILayout.Popup](https://docs.unity3d.com/ScriptReference/EditorGUILayout.Popup.html)

</details>
